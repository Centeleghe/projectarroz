package br.org.luiserduardo.interfaces;

import br.org.luiseduardo.classes.Cliente;
import br.org.luiseduardo.classes.ClienteRepositorio;
import br.org.luiseduardo.classes.Logger;
import br.org.luiseduardo.classes.Produto;
import br.org.luiseduardo.classes.ProdutoRepositorio;
import br.org.luiseduardo.classes.ReadObject;
import br.org.luiseduardo.classes.Venda;
import br.org.luiseduardo.classes.VendaRepositorio;
import br.org.luiseduardo.classes.WriteObject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class TelaCadastroController implements Initializable {

    @FXML
    private TextField nomeTexto;
    @FXML
    private TextField precoTexto;
    @FXML
    private TextField pesoTexto;
    @FXML
    private TextField tipoTexto;
    @FXML
    private TextField quatidadeTexto;
    @FXML
    private Button cadastrarButton;
    @FXML
    private TableView<Produto> tabelaPrincipal;
    @FXML
    private TableColumn<Produto, String> nomeColunaPrincipal;
    @FXML
    private TableColumn<Produto, Double> precoColunaPrincipal;
    @FXML
    private TableColumn<Produto, Double> pesoColunaPrincipal;
    @FXML
    private TableColumn<Produto, String> tipoColunaPrincipal;
    @FXML
    private TableColumn<Produto, Integer> quantidadeColunaPrincipal;
    @FXML
    private TableView<Produto> tabelaCarrinho;
    @FXML
    private TableColumn<Produto, String> nomeColunaCarrinho;
    @FXML
    private TableColumn<Produto, Double> precoColunaCarrinho;
    @FXML
    private TableColumn<Produto, Double> pesoColunaCarrinho;
    @FXML
    private TableColumn<Produto, String> tipoColunaCarrinho;
    @FXML
    private TableColumn<Produto, Integer> quantidadeColunaCarrinho;
    @FXML
    private Button carrinhoButton;
    @FXML
    private Button comprarButton;
    @FXML
    private TextField idClienteTexto;
    @FXML
    private TextField vendedorTexto;
    @FXML
    private Button cadastrarClienteButton;
    @FXML
    private TextField descontoTexto;
    @FXML
    private Button cadastrarButton1;
    @FXML
    private DatePicker dataCampo;
    
    private ArrayList<Cliente> clientes = new ArrayList<>();
    private ClienteRepositorio clienteRepositorio = new ClienteRepositorio();
    private ObservableList<Produto> produtosPrincipais;
    public ObservableList<Produto> produtosCarrinho;
    public static ArrayList<Produto> produtosCarrinhoBuffer = new ArrayList<>();
    private ProdutoRepositorio produtoRepositorio = new ProdutoRepositorio();
    private Logger logger = new Logger();
    private ReadObject readObject = new ReadObject();
    private VendaRepositorio vendaRepositorio = new VendaRepositorio();
   
    
    @FXML
    public void clickCadastrar(){
        Produto produto = new Produto();
        produto.setNome(nomeTexto.getText());
        produto.setPeso(Double.parseDouble(pesoTexto.getText()));
        produto.setPreco(Double.parseDouble(pesoTexto.getText()));
        produto.setQuantidade(Integer.parseInt(quatidadeTexto.getText()));
        produto.setTipoArroz(tipoTexto.getText());
        
        limparTela();
        produtoRepositorio.inserirProduto(produto);
        produtosPrincipais.add(produto);
    }
    
    @FXML
    public void clickAdicionarCarrinhoButton(){
        Produto produtoPrincipal = tabelaPrincipal.getSelectionModel().getSelectedItem();
        int id = getIdCarrinho(produtoPrincipal);
        if(id != -1){
            Produto produtoCarrinho = produtosCarrinho.get(id);
            produtoCarrinho.setQuantidade(produtoCarrinho.getQuantidade() + 1);
            
            Produto produtoCarrinhoBuffer = produtosCarrinhoBuffer.get(id);
            produtoCarrinhoBuffer.setQuantidade(produtoCarrinho.getQuantidade());
        }
        else{
            Produto produtoCarrinho = new Produto();
            carregarProduto(produtoCarrinho, produtoPrincipal);
            produtosCarrinho.add(produtoCarrinho);
            produtosCarrinhoBuffer.add(produtoCarrinho);
        }
        produtoPrincipal.setQuantidade(produtoPrincipal.getQuantidade()-1);
        tabelaCarrinho.refresh();
        tabelaPrincipal.refresh();
    }
    
    @FXML
    public void clickComprarButton(){
        if(idExiste(Integer.parseInt(idClienteTexto.getText()))){
            Venda venda = new Venda();
            venda.setDesconto(Double.parseDouble(descontoTexto.getText()));
            venda.setVendedor(vendedorTexto.getText());
            venda.setData(java.sql.Date.valueOf(dataCampo.getValue()));
            vendaRepositorio.cadastrar(venda, getCliente(Integer.parseInt(idClienteTexto.getText())));
            
            for(Produto produto : produtosPrincipais){
                int id = getIdCarrinho(produto);
                if(id != -1){
                    produtoRepositorio.update(produto);
                }
            }
            tabelaPrincipal.refresh();
            produtosCarrinho.clear();
            tabelaCarrinho.refresh();
            produtosCarrinhoBuffer.clear();
            limparTela();
            excluirArquivo();
        }else{
            
        }   
    }
    
    @FXML
    public void clickCadastrarUsuario(){
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("TelaCadastroUsuario.fxml"));
            root = loader.load();
            
            Stage stage = ProjectArroz.stage;

            Scene scene = new Scene(root);

            stage.setScene(scene);

        } catch (NullPointerException | IOException ex) {
            throw new RuntimeException("Erro: verifique o nome do arquivo FXML.", ex);
        }
    }
    
    private boolean idExiste(int id){
        boolean b = false;
        for(Cliente cliente : clientes){
            if(cliente.getId() == id){
                b = true;
            }
        }
        return b;
    }
    
    private Cliente getCliente(int id){
        Cliente clientePego = new Cliente();
        for(Cliente cliente : clientes){
            if(cliente.getId() == id){
                clientePego = cliente;
            }
        }
        return clientePego;
    }
    
    private void limparTela(){
        nomeTexto.clear();
        precoTexto.clear();
        pesoTexto.clear();
        tipoTexto.clear();
        quatidadeTexto.clear();
        vendedorTexto.clear();
        descontoTexto.clear();
        idClienteTexto.clear();
    }
    
    private void excluirArquivo(){
        File file = new File("produto.ser");
        file.delete();
    }
    
    public int getIdCarrinho(Produto produto){
     
        Produto produto1;
        for(int i = 0; i<produtosCarrinho.size(); i++){
            produto1 = produtosCarrinho.get(i);
            if(produto1.getId() == produto.getId()){
               return i;
            }
        }
        return -1;
    }
    
    public void carregarProduto(Produto produtoCarrinho, Produto produtoPrincipal){
        produtoCarrinho.setId(produtoPrincipal.getId());
        produtoCarrinho.setNome(produtoPrincipal.getNome());
        produtoCarrinho.setPeso(produtoPrincipal.getPeso());
        produtoCarrinho.setPreco(produtoPrincipal.getPreco());
        produtoCarrinho.setTipoArroz(produtoPrincipal.getTipoArroz());
        produtoCarrinho.setQuantidade(1);
    }
    

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        produtosPrincipais = tabelaPrincipal.getItems();
        produtosCarrinho = tabelaCarrinho.getItems();
        
        nomeColunaPrincipal.setCellValueFactory(new PropertyValueFactory<>("nome"));
        precoColunaPrincipal.setCellValueFactory(new PropertyValueFactory<>("preco"));
        pesoColunaPrincipal.setCellValueFactory(new PropertyValueFactory<>("peso"));
        tipoColunaPrincipal.setCellValueFactory(new PropertyValueFactory<>("tipoArroz"));
        quantidadeColunaPrincipal.setCellValueFactory(new PropertyValueFactory<>("quantidade"));
        
        nomeColunaCarrinho.setCellValueFactory(new PropertyValueFactory<>("nome"));
        precoColunaCarrinho.setCellValueFactory(new PropertyValueFactory<>("preco"));
        pesoColunaCarrinho.setCellValueFactory(new PropertyValueFactory<>("peso"));
        tipoColunaCarrinho.setCellValueFactory(new PropertyValueFactory<>("tipoArroz"));
        quantidadeColunaCarrinho.setCellValueFactory(new PropertyValueFactory<>("quantidade"));
        
        this.tabelaPrincipal.setItems(produtosPrincipais);
        this.tabelaCarrinho.setItems(produtosCarrinho);
        
        clientes = clienteRepositorio.getAll();
        
        ArrayList<Produto> ListaProdutos = produtoRepositorio.getAll();
        for(Produto produto : ListaProdutos){
            produtosPrincipais.add(produto);
        }
        readObject.lerObjetos();
        for(Produto produto : produtosCarrinhoBuffer){
            produtosCarrinho.add(produto);
        }
        
        for(Produto produtoPrincipal : produtosPrincipais){
            int id = getIdCarrinho(produtoPrincipal);
            if(id != -1){
                Produto produtoCarrinho = produtosCarrinho.get(id);
            
                produtoPrincipal.setQuantidade(produtoPrincipal.getQuantidade() - produtoCarrinho.getQuantidade());
            }
        }
    }
}
