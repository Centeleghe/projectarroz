package br.org.luiserduardo.interfaces;

import br.org.luiseduardo.classes.Cliente;
import br.org.luiseduardo.classes.ClienteRepositorio;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class TelaCadastroUsuarioController implements Initializable {

    @FXML
    private TextField nomeTexto;
    @FXML
    private TextField senhaTexto;
    @FXML
    private Button cadastrarButton;
    @FXML
    private Button voltarButton;
    
    private ClienteRepositorio clienteRepositorio = new ClienteRepositorio();
    
    @FXML
    public void clickCadastrar(){
        Cliente cliente = new Cliente();
        
        cliente.setNome(nomeTexto.getText());
        cliente.setSenha(senhaTexto.getText());
        
        clienteRepositorio.cadastrar(cliente);
    }
    
    @FXML
    public void clickVoltar(){
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("TelaCadastro.fxml"));
            root = loader.load();
            
            Stage stage = ProjectArroz.stage;

            Scene scene = new Scene(root);

            stage.setScene(scene);

        } catch (NullPointerException | IOException ex) {
            throw new RuntimeException("Erro: verifique o nome do arquivo FXML.", ex);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
}
