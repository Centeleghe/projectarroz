package br.org.luiseduardo.classes;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.collections.ObservableList;

public class WriteObject {
    private FileOutputStream file;
    private ObjectOutputStream objectOutputStream;
    
    public void escreverObjeto(ArrayList<Produto> produtos){
        try{
            file = new FileOutputStream("produto.ser");
            
            objectOutputStream = new ObjectOutputStream(file);
            
            objectOutputStream.writeObject(produtos);
         
            objectOutputStream.close();
       }catch(Exception ex){
           ex.printStackTrace();
       } 
    }
}
