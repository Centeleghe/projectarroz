package br.org.luiseduardo.classes;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    private File f;
    private FileWriter fw;

    public Logger() {
        f = new File("log.txt");      
        try {            
            fw = new FileWriter(f);
            fw.write("Arquivo gravado em : " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void escreve(String str) {
        try {
             fw.append("\n { data : '" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())+ "', registro: '"+ str +"'},");
             fw.flush();//Envia os dados para o arquivo
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
