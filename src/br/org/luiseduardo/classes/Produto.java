package br.org.luiseduardo.classes;

import java.io.Serializable;

public class Produto implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private int id;
    private double preco;
    private double peso;
    private String tipoArroz;
    private String nome;
    private int quantidade;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getTipoArroz() {
        return tipoArroz;
    }

    public void setTipoArroz(String tipoArroz) {
        this.tipoArroz = tipoArroz;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quatidade) {
        this.quantidade = quatidade;
    }
    
    
}
