package br.org.luiseduardo.classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ProdutoRepositorio {
    
    public void inserirProduto(Produto produto){
        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;

        String insertTableSQL = "INSERT INTO produto"
                + "(id_produto, nome_saco, tipo_arroz, peso_saco, preco_saco, quantidade) VALUES"
                + "(seq_produto_id_produto.nextval,?,?,?,?,?)";

        try {

            String generatedColumns[] = {"id_produto"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
            
            ps.setString(1, produto.getNome());
            ps.setString(2, produto.getTipoArroz());
            ps.setDouble(3, produto.getPeso());
            ps.setDouble(4, produto.getPreco());
            ps.setInt(5, produto.getQuantidade());
            ps.executeUpdate();


            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            produto.setId(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        
        ArrayList<Produto> produtos = new ArrayList<>();
        
        String selectSQL = "select * from produto";
        
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Produto produto = new Produto();
                
                produto.setId(rs.getInt("id_produto"));
                produto.setNome(rs.getString("nome_saco"));
                produto.setPeso(rs.getDouble("peso_saco"));
                produto.setPreco(rs.getDouble("preco_saco"));
                produto.setTipoArroz(rs.getString("tipo_arroz"));
                produto.setQuantidade(rs.getInt("quantidade"));
                
                produtos.add(produto);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return produtos;
    }
    
    public void update(Produto produto){
         Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        
        String insertTableSQL = "update produto " + "set quantidade = ? "
                + "where id_produto = ?";
        
        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            
            ps.setInt(1, produto.getQuantidade());
            ps.setInt(2, produto.getId());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
}