package br.org.luiseduardo.classes;

import br.org.luiserduardo.interfaces.TelaCadastroController;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class ReadObject {
    private FileInputStream fileInputStream;
    private ObjectInputStream objectInputStream;
    
    public void lerObjetos(){
        try{
            fileInputStream = new FileInputStream("produto.ser");
            
            objectInputStream = new ObjectInputStream(fileInputStream);

            
            TelaCadastroController.produtosCarrinhoBuffer = (ArrayList<Produto>) objectInputStream.readObject();
            objectInputStream.close();
        
        }catch(FileNotFoundException ex){
            System.out.println("Não há arquivo");
       }catch(Exception ex){
            ex.printStackTrace();
       }
    }
}
