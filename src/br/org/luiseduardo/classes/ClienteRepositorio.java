package br.org.luiseduardo.classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ClienteRepositorio {
    public void cadastrar(Cliente cliente){
        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;

        String insertTableSQL = "INSERT INTO cliente "
                + "(id_cliente, nome_cliente, senha_cliente) VALUES"
                + "(seq_cliente_id_cliente.nextval,?,?)";

        try {

            String generatedColumns[] = {"id_cliente"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
            
            ps.setString(1, cliente.getNome());
            ps.setString(2, cliente.getSenha());
            ps.executeUpdate();


            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            cliente.setId(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        
        ArrayList<Cliente> clientes = new ArrayList<>();
        
        String selectSQL = "select * from cliente";
        
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Cliente cliente = new Cliente();
                
                cliente.setId(rs.getInt("id_cliente"));
                cliente.setNome(rs.getString("nome_cliente"));
                cliente.setSenha(rs.getString("senha_cliente"));
                
                clientes.add(cliente);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return clientes;
    }
}
