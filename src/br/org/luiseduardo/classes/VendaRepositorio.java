package br.org.luiseduardo.classes;

import br.org.luiserduardo.interfaces.TelaCadastroController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class VendaRepositorio {
    
    public void cadastrar(Venda venda, Cliente cliente){
        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;

        String insertTableSQL = "INSERT INTO venda "
                + "(id_venda, data_venda, desconto_venda, vendedor_venda, id_cliente) VALUES"
                + "(seq_venda_id_venda.nextval,?,?,?,?)";

        try {

            String generatedColumns[] = {"id_venda"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
            
            ps.setDate(1, venda.getData());
            ps.setDouble(2, venda.getDesconto());
            ps.setString(3, venda.getVendedor());
            ps.setInt(4, cliente.getId());
            ps.executeUpdate();


            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            venda.setId(chaveGerada);
            cadastrarProdutoVenda(venda);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void cadastrarProdutoVenda(Venda venda){
        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;

        String insertTableSQL = "INSERT INTO produto_venda "
                + "(id_venda, id_produto, preco) VALUES"
                + "(?,?,?)";
        for(Produto produto : TelaCadastroController.produtosCarrinhoBuffer){
            Double precoTotal = produto.getQuantidade()*produto.getPreco();
            precoTotal = precoTotal - venda.getDesconto();
            try {
           
                ps = dbConnection.prepareStatement(insertTableSQL);

                st = dbConnection.createStatement();

                ps.setInt(1, venda.getId());
                ps.setInt(2, produto.getId());
                ps.setDouble(3, precoTotal);
                ps.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
