package br.org.luiseduardo.classes;

import java.io.Serializable;

/**
 * Model class of PRODUTO_VENDA.
 * 
 * @author generated by ERMaster
 * @version $Id$
 */
public class ProdutoVenda implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** PRODUTO. */
	private Produto produto;

	/** VENDA. */
	private Venda venda;

	/** PRECO. */
	private  preco;

	/**
	 * Constructor.
	 */
	public ProdutoVenda() {
	}

	/**
	 * Set the PRODUTO.
	 * 
	 * @param produto
	 *            PRODUTO
	 */
	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	/**
	 * Get the PRODUTO.
	 * 
	 * @return PRODUTO
	 */
	public Produto getProduto() {
		return this.produto;
	}

	/**
	 * Set the VENDA.
	 * 
	 * @param venda
	 *            VENDA
	 */
	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	/**
	 * Get the VENDA.
	 * 
	 * @return VENDA
	 */
	public Venda getVenda() {
		return this.venda;
	}

	/**
	 * Set the PRECO.
	 * 
	 * @param preco
	 *            PRECO
	 */
	public void setPreco( preco) {
		this.preco = preco;
	}

	/**
	 * Get the PRECO.
	 * 
	 * @return PRECO
	 */
	public  getPreco() {
		return this.preco;
	}


}
